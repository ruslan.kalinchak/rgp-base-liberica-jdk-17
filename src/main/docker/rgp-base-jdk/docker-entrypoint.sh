#!/bin/bash
## The entrypoint is responsible for setting up all the JAVA_OPTS that this service will run with
set -e

if [ -z "$SERVICE_JAR_FILE" ]; then
	echo >&2 'error: $SERVICE_JAR_FILE must be set'
	exit 1
fi

DEBUG_OPTS=""

if [ "$JVM_DEBUG_ENABLED" == "true" ]; then
	DEBUG_OPTS="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005"
fi

exec java \
$JAVA_OPTS \
-jar $SERVICE_JAR_FILE \
"$@"
